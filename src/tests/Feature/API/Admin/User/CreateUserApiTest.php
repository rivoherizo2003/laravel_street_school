<?php


namespace Tests\Feature\API\Admin\User;

use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;
use Symfony\Component\HttpFoundation\Cookie;
use Tests\TestCase;

class CreateUserApiTest extends TestCase
{
    use RefreshDatabase;

    public function test_create_user_with_success(): void
    {

        $user = User::factory()->create();
        $response = $this
            ->actingAs($user, 'sanctum')
            ->postJson('api/admin/create/user', [
                'name'     => 'user test name',
                'email'    => 'test@gmail.com',
                'password' => 'admin78_UU',
                'password_confirmation' => 'admin78_UU'
            ]);

        $userCreated = DB::table('users')->where('email','test@gmail.com')->first();

        $response
            ->assertStatus(Response::HTTP_CREATED)
            ->assertJson([
                'isSuccessful' => true,
            ]);

        $this->assertEquals('test@gmail.com', $userCreated->email);
    }

    public function test_create_user_with_forbidden_access(): void
    {
        $response = $this
            ->postJson('api/admin/create/user', [
                'name'     => 'user test name',
                'email'    => 'test@gmail.com',
                'password' => 'admin78_UU',
                'password_confirmation' => 'admin78_UU'
            ]);

        $response
            ->assertStatus(Response::HTTP_UNAUTHORIZED);
    }
}
