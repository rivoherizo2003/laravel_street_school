<?php

namespace Tests\Unit\Services\Admin\User;

use App\Models\User;
use App\Services\Admin\User\SaveEditUserService;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class SaveEditUserServiceTest extends TestCase
{
    /**
     * @dataProvider provideValidData
     */
    public function test_save_edit_user_with_success(array $data): void
    {
        $userMock = $this->createStub(User::class);
        $mock = $this->createMock(SaveEditUserService::class);
        $user = $mock->execute($data, $userMock);

        self::assertInstanceOf(User::class, $user);
    }

    public function provideValidData():array
    {
        return [
            [[
                 'name' => 'test name_chrono',
                 'email' => 'test@gmail.com',
             ]],
            [[
                 'name' => 'name_chrono',
                 'email' => 'test456789@gmail.com',
             ]]
        ];
    }
}
