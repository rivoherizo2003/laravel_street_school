<?php


namespace App\Services\Admin\User;


use App\Http\Requests\Users\UserCreateRequest;
use App\Models\User;

class CreateUserService
{
    public function execute(array $userData):User
    {
        return User::create([
            'name' => $userData['name'],
            'email' => $userData['email'],
            'password' => $userData['password']
        ]);
    }
}
