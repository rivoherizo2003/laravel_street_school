<?php

use App\Http\Controllers\API\Admin\User\CreateUserApi;
use App\Http\Controllers\API\Admin\User\DeleteUserApi;
use App\Http\Controllers\API\Admin\User\SaveEditUserApi;
use App\Http\Controllers\API\Admin\User\GetUsersApi;
use App\Http\Controllers\API\Admin\Auth\AuthenticateApi;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::prefix('/admin')->group(function(){
    Route::get("/list/users", GetUsersApi::class)
        ->name("api.list.users");
    Route::post("/create/user", CreateUserApi::class)
        ->name("api.create.user");
    Route::patch("/edit/user/{id}", SaveEditUserApi::class)
    ->name("api.edit.user");
    Route::delete("/delete/user/{id}", DeleteUserApi::class)
        ->name("api.delete.user");
});

Route::prefix('/public')->group(function (){
    Route::post('/authenticate', AuthenticateApi::class)
        ->name("api.authenticate.user");
});

