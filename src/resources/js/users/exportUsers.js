import $ from 'jquery';
import Swal from 'admin-lte/plugins/sweetalert2/sweetalert2.min';

const Toast = Swal.mixin({
    toast: true,
    position: 'top-end',
    showConfirmButton: false,
    timer: 8000
});

$(function (){
    const btnExported = $('#a-exported-csv');

    btnExported.click(function (){
        $.ajax({
            type: 'GET',
            url: $(this).data('href'),
            success: function (){
                Toast.fire({
                    icon: 'info',
                    text: 'You will be notified if the user\'s list was sent to your email!',
                    timer: 6000
                });
            }
        })
   });
});
