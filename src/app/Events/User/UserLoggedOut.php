<?php

namespace App\Events\User;

use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithBroadcasting;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Contracts\Broadcasting\ShouldBroadcastNow;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\SerializesModels;

class UserLoggedOut implements ShouldBroadcastNow
{
    use Dispatchable,InteractsWithBroadcasting, SerializesModels;

    public string $username;

    public string $typeIcon = "warning";

    public string $message = " has logged out";

    /**
     * Create a new event instance.
     */
    public function __construct(string $username)
    {
        $this->username = $username;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return Channel
     */
    public function broadcastOn(): Channel
    {
        return new Channel('public');
    }

    public function broadcastAs():string
    {
        return 'user.logged.out';
    }

    public function broadcastWith(): array
    {
        return ['username' => $this->username, 'typeIcon' => $this->typeIcon, 'message' => $this->username.$this->message];
    }
}
