<?php

namespace App\Http\Controllers\Admin\PubSub;

use App\Http\Controllers\Controller;
use App\Http\Requests\Redis\PublishRequest;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redis;

class PublishMsg extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth', 'verified']);
    }

    /**
     * Undocumented function
     *
     * @param PublishRequest $publishRequest
     * @return void
     */
    public function __invoke(PublishRequest $publishRequest)
    {
        $validated = $publishRequest->validated();
        $name = Auth::user()->name;
        $msg = $name.' > '.$validated['message'];
        Redis::publish('test-channel', $msg);

        return redirect()->route('show-channel');
    }
}
