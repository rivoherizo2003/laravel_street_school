import 'admin-lte/plugins/jquery/jquery.min';
import 'admin-lte/plugins/bootstrap/js/bootstrap.bundle.min';
import 'admin-lte/plugins/overlayScrollbars/js/jquery.overlayScrollbars.min';
import 'admin-lte/plugins/select2/js/select2.full.min';
import 'admin-lte/plugins/toastr/toastr.min';
import 'admin-lte/dist/js/adminlte.min';
import 'admin-lte/plugins/chart.js/Chart.min';
import './bootstrap';

import.meta.glob([
    '../images/**',
    '../fonts/**',
]);
