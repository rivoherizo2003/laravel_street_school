<?php

namespace App\Jobs\Admin;

use App\Jobs\Middleware\RateLimited;
use App\Services\Admin\UserCsvProcessor;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Redis;

class ProcessUsersCsv implements ShouldQueue, ShouldBeUnique
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private int $idUserConnected;

    /**
     * Create a new job instance.
     */
    public function __construct(int $idUserConnected)
    {
        //
        $this->idUserConnected = $idUserConnected;
    }

    /**
     * Execute the job.
     */
    public function handle(UserCsvProcessor $userCsvProcessor): void
    {
        $userCsvProcessor->execute($this->idUserConnected);
    }

    /**
     * Undocumented function
     *
     * @return array<int, object>
     */
    public function middleware():array
    {
        return [new RateLimited];
    }
}
