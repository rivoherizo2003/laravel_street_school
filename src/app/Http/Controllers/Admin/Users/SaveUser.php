<?php

namespace App\Http\Controllers\Admin\Users;

use App\Http\Controllers\Controller;
use App\Http\Requests\Users\UserEditRequest;
use App\Models\User;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\Hash;

class SaveUser extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth','verified']);
    }

    /**
     * Action to save user
     *
     * @param UserEditRequest $userEditRequest
     * @return RedirectResponse
     */
    public function __invoke(UserEditRequest $userEditRequest): RedirectResponse
    {
        $validated = $userEditRequest->validated();

        $user = User::find($userEditRequest->id);
        $user->name = $validated['name'];
        $user->email = $validated['email'];
        $user->password = Hash::make($validated['password']);

        $user->save();

        return redirect()->route('list-users');
    }
}
