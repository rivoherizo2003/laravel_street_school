<?php


namespace App\Events\User;


use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithBroadcasting;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Contracts\Broadcasting\ShouldBroadcastNow;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\SerializesModels;

class ListUsersExported implements ShouldBroadcastNow
{
    use Dispatchable,InteractsWithBroadcasting, SerializesModels;

    public string $typeIcon = "success";

    public string $message = " List users exported! Check your email please.";
    private int $userId;

    /**
     * Create a new event instance.
     */
    public function __construct(int $userId)
    {
        $this->userId = $userId;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return PrivateChannel
     */
    public function broadcastOn(): PrivateChannel
    {
        return new PrivateChannel('App.Models.User.'.$this->userId);
    }

    public function broadcastWith(): array
    {
        return ['typeIcon' => $this->typeIcon, 'message' => $this->message];
    }

    public function broadcastAs():string
    {
        return 'list.users.exported';
    }
}
