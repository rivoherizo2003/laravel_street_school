<?php

namespace Tests\Feature\API\Admin\User;

use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\Response;
use Tests\TestCase;

class SaveEditUserApiTest extends TestCase
{
    use RefreshDatabase;

    public function test_edit_user_with_success(): void
    {
        $user = User::factory()->create();
        $userToEdit = User::factory(['email' => 'testedit@gmail.com','name' => 'nametestici'])->create();

        $response = $this
            ->actingAs($user, 'sanctum')
            ->patchJson('api/admin/edit/user/'.$userToEdit->id, [
            'name' => 'name edited here',
            'email' => 'testedit@gmail.com',
            'uuid' => $userToEdit->uuid
        ]);

        $userToEdit = $userToEdit->refresh();
        $this->assertEquals('testedit@gmail.com', $userToEdit->email, 'Email should be equals to testedit@gmail.com');
        $this->assertEquals('name edited here', $userToEdit->name, "Name should be equals to [name edited here]");


        $response->assertStatus(Response::HTTP_OK);
    }
}
