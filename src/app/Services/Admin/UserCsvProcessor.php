<?php

namespace App\Services\Admin;

use App\Events\User\ListUsersExported;
use App\Mail\ExportCsvUsers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;

class UserCsvProcessor
{
    public function execute(int $userIdConnected)
    {
        sleep(10);
        Mail::to($userIdConnected.'herizo@test.com')->send(new ExportCsvUsers());

        broadcast(new ListUsersExported($userIdConnected));
    }
}
