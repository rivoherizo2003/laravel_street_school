<?php

namespace App\Http\Controllers\API\Admin\User;

use App\Http\Controllers\Controller;
use App\Http\Requests\Users\EditUserRequest;
use App\Models\User;
use App\Services\Admin\User\SaveEditUserService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class SaveEditUserApi extends Controller
{
    private SaveEditUserService $saveEditUserService;

    public function __construct(SaveEditUserService $saveEditUserService)
    {
        $this->middleware("auth:sanctum");
        $this->saveEditUserService = $saveEditUserService;
    }

    /**
     * @param EditUserRequest $editUserRequest
     * @return JsonResponse
     */
    public function __invoke(EditUserRequest $editUserRequest):JsonResponse
    {
        $validatedEditUserReq = $editUserRequest->validated();
        $user = $this->saveEditUserService->execute($validatedEditUserReq);

        return response()
            ->json(["isSuccessfull" => true]);
    }
}
