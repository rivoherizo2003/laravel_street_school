#!/bin/bash
echo -e "=============================\n"
echo -e "Clearing cache configuration\n"
docker compose run --rm artisan config:clear
echo -e "=============================\n"
echo -e "Clearing cache application\n"
docker compose run --rm artisan cache:clear
echo -e "=============================\n"
echo -e "Launching queue work\n"
docker compose run --rm artisan queue:work --stop-when-empty
echo -e "=============================\n"
