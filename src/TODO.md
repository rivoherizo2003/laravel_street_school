## SECURITY
- [x] Login page
- [x] Setup authentication middleware

## <h1>List of tasks</h1>
- [x] Real time notification
- [x] Jobs queuing

## <h1>API</h1>
- [x] Get user's list
- [x] Create new user
- [] Edit existing user
