<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redis;
use Illuminate\Support\Facades\View;

class ShowChannel extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth', 'verified']);
    }

    /**
     * Handle the incoming request.
     */
    public function __invoke(Request $request)
    {
        // $sizeList = Redis::command('LLEN', ['messages']);
        $messages = Redis::command('LRANGE', ['messages',0, -1]);

        return View::make('components.admin.channel.channel', ['messages' => $messages]);
    }
}
