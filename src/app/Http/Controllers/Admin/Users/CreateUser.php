<?php

namespace App\Http\Controllers\Admin\Users;

use App\Http\Controllers\Controller;
use App\Http\Requests\Users\UserCreateRequest;
use App\Models\User;
use Illuminate\Http\RedirectResponse;

class CreateUser extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth', 'verified']);
    }

    /**
     * @param UserCreateRequest $createUserRequest
     * @return RedirectResponse
     */
    public function __invoke(UserCreateRequest $createUserRequest): RedirectResponse
    {
        $validated = $createUserRequest->validated();
        User::create([
            'name' => $validated['name'],
            'email' => $validated['email'],
            'password' => $validated['password']
        ]);

        return redirect()->route('list-users');
    }
}
