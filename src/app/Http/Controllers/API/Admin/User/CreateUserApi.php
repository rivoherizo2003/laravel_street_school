<?php


namespace App\Http\Controllers\API\Admin\User;


use App\Http\Controllers\Controller;
use App\Http\Requests\Users\UserCreateRequest;
use App\Services\Admin\User\CreateUserService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Response;
use League\Uri\Http;

class CreateUserApi extends Controller
{
    private CreateUserService $createUserService;

    public function __construct(CreateUserService $createUserService)
    {
        $this->middleware(['auth:sanctum']);
        $this->createUserService = $createUserService;
    }

    public function __invoke(UserCreateRequest $userCreateRequest): JsonResponse
    {
        $validatedUserCreateRequest = $userCreateRequest->validated();
        $userCreated = $this->createUserService->execute($validatedUserCreateRequest);

        return response()
            ->json(['isSuccessful' => true, 'email' => $userCreated->email ], Response::HTTP_CREATED);
    }
}
