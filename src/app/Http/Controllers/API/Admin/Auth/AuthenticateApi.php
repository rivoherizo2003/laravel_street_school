<?php

namespace App\Http\Controllers\API\Admin\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;

class AuthenticateApi extends Controller
{
    const BAD_CREDENTIAL = ['isAuthenticated' => false, 'message' => 'Bad credentials'];

    /**
     * Handle the incoming request.
     */
    public function __invoke(Request $request): JsonResponse
    {
        $credentials = $request->validate([
            'email'    => ['required', 'email'],
            'password' => ['required']
        ]);

        if (Auth::attempt($credentials)) {
            $request->session()->regenerate();

            return response()->json([
                'isAuthenticated' => true
            ], Response::HTTP_ACCEPTED);
        }

        return response()
            ->json(self::BAD_CREDENTIAL, Response::HTTP_UNAUTHORIZED);
    }
}
