<?php

namespace Tests\Feature\API;

use App\Http\Controllers\API\Admin\Auth\AuthenticateApi;
use App\Models\User;
use Illuminate\Contracts\Auth\Authenticatable as UserContract;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;
use Symfony\Component\HttpFoundation\Cookie;
use Tests\TestCase;

class AuthenticateApiTest extends TestCase
{
    use RefreshDatabase;

    protected string|null $xsrfToken = null;

    public function test_successful_authentication(): void
    {
        $response = $this->get('/sanctum/csrf-cookie');
        $response->assertStatus(204);
        $cookies = $response?->baseResponse?->headers?->getCookies();
        /**
         * @var  $key
         * @var Cookie $cookie
         */
        foreach ($cookies as $key => $cookie) {
            if ($cookie->getName() == "XSRF-TOKEN") {
                $this->xsrfToken = $cookie->getValue();
                break;
            }
        }
        $this->assertNotNull($this->xsrfToken, "XSRF-TOKEN should not be null");

        $user = User::factory(['email' => 'test@gmail.com'])->create();
        $response = $this->postJson("/api/public/authenticate", [
            'email'    => 'test@gmail.com',
            'password' => 'password'
        ]);

        $response->assertStatus(Response::HTTP_ACCEPTED)
            ->assertExactJson(['isAuthenticated' => true]);

        $response = $this->get("/api/admin/list/users", [
            'X-XSRF-TOKEN' => $this->xsrfToken
        ]);

        $response->assertStatus(Response::HTTP_OK);
    }

    public function test_bad_credential_authentication():void
    {
        $response = $this->postJson("/api/public/authenticate", [
            'email' => 'herizo@gmail.com',
            'password' => 'password'
        ]);

        $response->assertStatus(Response::HTTP_UNAUTHORIZED)
        ->assertExactJson(AuthenticateApi::BAD_CREDENTIAL);
    }
}
