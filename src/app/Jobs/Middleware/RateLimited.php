<?php

namespace App\Jobs\Middleware;

use Closure;
use Illuminate\Contracts\Redis\LimiterTimeoutException;
use Illuminate\Support\Facades\Redis;

class RateLimited
{
    /**
     * Undocumented function
     *
     * @param object $job
     * @param Closure $next
     * @return void
     * @throws LimiterTimeoutException
     */
    public function handle(object $job, Closure $next):void
    {
        Redis::throttle('key')
        ->block(0)->allow(1)->every(5)
        ->then(function() use($job, $next){
            $next($job);
        }, function() use($job){
            $job->release(5);
        });
    }
}
