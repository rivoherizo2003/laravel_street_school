<?php


namespace Tests\Feature\Services\Admin\User;



use App\Http\Requests\Users\UserCreateRequest;
use App\Models\User;
use App\Services\Admin\User\CreateUserService;
use Illuminate\Support\Facades\Validator;
use Mockery\Mock;
use Mockery\MockInterface;
use PHPUnit\Framework\MockObject\Exception;
use Tests\TestCase;

class CreateUserServiceTest extends TestCase
{
    /**
     * @dataProvider provideValidData
     * @param array $data
     * @throws Exception
     */
    public function test_create_user_with_success(array $data):void
    {
        $mock = $this->createMock(CreateUserService::class);
        $user = $mock->execute($data);

        self::assertInstanceOf(User::class, $user);
    }

    public function provideValidData():array
    {
        return [
            [[
                'name' => 'test name_chrono',
                'email' => 'test@gmail.com',
                'password' => 'admin78_UU',
                'password_confirmation' => 'admin78_UU',
             ]],
            [[
                 'name' => 'name_chrono',
                 'email' => 'test456789@gmail.com',
                 'password' => 'admin78_UU_iiuyiu',
                 'password_confirmation' => 'admin78_UU_iiuyiu',
             ]]
        ];
    }
}
