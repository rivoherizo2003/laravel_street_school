#!/bin/bash
#!/bin/bash
echo -e "=============================\n"
echo -e "Clearing cache configuration\n"
docker compose run --rm artisan config:clear
echo -e "=============================\n"
echo -e "Clearing cache application\n"
docker compose run --rm artisan cache:clear
echo -e "=============================\n"
if ! [ -n "$1" ]; then
  echo -e "Launching application's tests\n"
  docker compose run --rm artisan test
else
  echo -e "Launching application's tests for $1\n"
  docker compose run --rm artisan test --filter=$1
fi