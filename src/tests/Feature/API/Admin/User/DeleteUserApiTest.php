<?php

namespace Tests\Feature\API\Admin\User;

use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class DeleteUserApiTest extends TestCase
{
    use RefreshDatabase;

    public function test_delete_user_successful(): void
    {
        $user = User::factory()->create();
        $userToDelete = User::factory(['email' => 'testedit@gmail.com','name' => 'nametestici'])->create();
        $response = $this
            ->actingAs($user, 'sanctum')
            ->delete('/api/admin/delete/user/'.$userToDelete->uuid);
        $response->assertStatus(200);
    }
}
