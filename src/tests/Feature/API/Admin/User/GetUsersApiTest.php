<?php

namespace Tests\Feature\API\Admin\User;

use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Testing\Fluent\AssertableJson;
use Tests\TestCase;
use function Monolog\toArray;

class GetUsersApiTest extends TestCase
{
    use RefreshDatabase;

    /**
     * A basic feature test example.
     */
    public function test_api_get_list_users_return_data(): void
    {
        $user = User::factory()->create();
        User::factory()->count(5)->create();
        $users = User::all();
        $response = $this
            ->actingAs($user, 'sanctum')
            ->get('/api/admin/list/users');

        $response
            ->assertStatus(200)
            ->assertJsonCount(2, 'data');

        //test page 2
        $response = $this
            ->actingAs($user)
            ->get('/api/admin/list/users?page=2');

        $response
            ->assertStatus(200)
            ->assertJsonCount(2, 'data')
            ->assertJson(fn(AssertableJson $json) => $json->has('links')->etc());
    }

    public function test_pagination_list_users():void
    {
        $user = User::factory()->create();
        User::factory()->count(5)->create();

        //test page 2
        $response = $this
            ->actingAs($user, 'sanctum')
            ->get('/api/admin/list/users?page=1');

        $response
            ->assertStatus(200)
            ->assertJsonCount(2, 'data')
            ->assertJson(fn(AssertableJson $json) => $json->has('links')->etc());

        //test page 2
        $response = $this
            ->actingAs($user)
            ->get('/api/admin/list/users?page=2');

        $response
            ->assertStatus(200)
            ->assertJsonCount(2, 'data')
            ->assertJson(fn(AssertableJson $json) => $json->has('links')->etc());

    }
}
