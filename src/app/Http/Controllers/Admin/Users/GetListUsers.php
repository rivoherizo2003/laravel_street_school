<?php

namespace App\Http\Controllers\Admin\Users;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\View;

class GetListUsers extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth', 'verified']);
    }

    /**
     * Handle the incoming request.
     */
    public function __invoke(Request $request): \Illuminate\Contracts\View\View
    {
//        $this->test();
        return View::make('components.admin.users.users', ['users' => DB::table('users')
            ->whereNull(['deleted_at'])
            ->orderBy('id')
            ->cursorPaginate(6)]);
    }

    public function test()
    {
        $users = DB::table('users')
            ->whereNull(['deleted_at'])
            ->orderBy('id')
            ->cursorPaginate(6);
//        $users = User::all('email');
        foreach ($users as $user ){
            echo $user->email."\n";
        }
        die;
    }
}
