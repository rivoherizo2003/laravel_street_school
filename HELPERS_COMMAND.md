## Install vendor
```bash
docker compose run lrv_composer install
```

## Build assets
```bash
docker compose run --rm npm -i run build
```

## Create controller
```bash
docker compose run --rm artisan make:controller CreateUser --invokable
```

## Create a component in sub folder
```bash
docker compose run --rm artisan make:component front/Home
```

## Run migration
```bash
docker compose run --rm artisan migrate [--env=testing]
```

## Create seeder for the User model
```bash
docker compose run --rm artisan make:seeder UserSeeder
```

## Launch seeder for the User model
```bash
docker compose run --rm artisan db:seed UserSeeder
```

## Launch queue worker and stop if empty
```bash
docker compose run --rm artisan queue:work --stop-when-empty
```

## Launch test for one file
```bash
docker compose run --rm artisan test --filter='Tests\\Feature\\API\\GetUsersControllerTest'
```

## Create test
```bash
docker compose run --rm artisan make:test "Tests\\Feature\\Services\\Admin\\User\\EditUserFromRequestTest"
```
