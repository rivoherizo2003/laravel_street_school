<?php


namespace App\Http\Requests\Users;


class EditUserRequest extends UserRequest
{
    public function messages()
    {
        $messages = parent::messages();
        return array_merge(['uuid.required' => 'You must provide the id of the user']);
    }

    public function rules(): array
    {
        return [
            'uuid' => 'required',
            'name' => 'required|min:10',
            'email' => 'required|email:rfc,dns|unique:users,email,'.$this->id,
        ];
    }
}
