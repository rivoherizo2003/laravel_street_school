<?php


namespace App\Services\Admin\User;


use App\Models\User;

class SaveEditUserService
{
    public function execute(array $data): User
    {
        $user = User::firstWhere('uuid',$data["uuid"]);

        $user->name = $data['name'];
        $user->email = $data['email'];

        $user->save();

        return $user->refresh();
    }
}
