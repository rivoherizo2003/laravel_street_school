## Requirements
<ul>
<li>Assume that you have docker engine installed on your environment</li>
</ul>

## Start containers

```bash
docker-compose up -d
```

## Install laravel dependencies via composer container

```bash
docker compose run --rm lrv_composer install
```

## Launch migrations to create our tables

```bash
docker compose run --rm artisan migrate
```

## Launch seeder for the User model
```bash
docker compose run --rm artisan db:seed UserSeeder
```

## Install packages from package.json

```bash
docker compose run --rm npm install
```

## Build our assets

```bash
docker compose run --rm npm -i run build
```

## look at our laravel application

<a href="http://localhost:8000"> Street school </a>

<h1>Enjoy!!</h1>
