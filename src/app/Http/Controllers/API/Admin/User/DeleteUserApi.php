<?php

namespace App\Http\Controllers\API\Admin\User;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class DeleteUserApi extends Controller
{
    public function __construct()
    {
        $this->middleware("auth:sanctum");
    }

    /**
     * Handle the incoming request.
     */
    public function __invoke(Request $request)
    {
        return response()
            ->json(['isSuccessful' => true]);
    }
}
