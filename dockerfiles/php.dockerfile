FROM php:8.1-fpm-alpine

WORKDIR /var/www/html

COPY src .

RUN apk add --no-cache --update --virtual buildDeps autoconf g++ make linux-headers\ 
 && pecl install xdebug \
 && docker-php-ext-enable xdebug \
 && pecl install -o -f redis \
 && docker-php-ext-enable redis \
 && docker-php-ext-install pdo pdo_mysql \
 && rm -rf /tmp/pear \
 && apk del buildDeps

RUN addgroup -g 1000 laravel && adduser -G laravel -g laravel -s /bin/sh -D laravel

USER laravel