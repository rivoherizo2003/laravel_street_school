<?php

namespace App\Http\Controllers\Admin\PubSub;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class Psubscribe extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth', 'verified']);
    }

    /**
     * Handle the incoming request.
     */
    public function __invoke(Request $request)
    {
        //
    }
}
